#set working directory 
setwd("/home/twillia2/datastore/taylor-lab/THOMAS/post_COVID19_landscape/submission_LCAH/methods/")
#read in population data
#import population data
population_data <- read.table("data/population_by_age_scotland_2016_2020.txt", 
                              header = T)

#read in total mortality data
total_mortality <- read.table("data/paediatric_mortality_weeks_13_to_30_2016_to_2020.txt",
                              header = TRUE)
normalised_deaths <- merge(total_mortality,population_data, by="year")

#create normalised mortality plot
normalised_deaths$norm_zero_to_four <- 100000*(normalised_deaths$under_fives/normalised_deaths$zero_to_four)
normalised_deaths$norm_five_to_fourteen <- 100000*(normalised_deaths$five_to_fourteen.x/normalised_deaths$five_to_fourteen.y)
normalised_deaths$norm_total <- 100000*(normalised_deaths$total/normalised_deaths$zero_to_fourteen)

#create plot for calculations
normalised_deaths_for_calculations <- normalised_deaths[,c(9:11)]

#write table
write.table(file = "data/normalised_deaths_for_plotting.txt",normalised_deaths_for_calculations,sep = "\t", quote = F )

#calculate z-scores and p-values

#all ages
previous_years_total <- normalised_deaths_for_calculations$norm_total[1:4]
mean_previous_years_total <- mean(previous_years_total)
sd_previous_years_total <- sd(previous_years_total)
z_score_total <- (normalised_deaths_for_calculations$norm_total[5]-mean_previous_years_total)/sd_previous_years_total
p_value_total <- pnorm(-abs(z_score_total))*2

#0-4
previous_years_0_4 <- normalised_deaths_for_calculations$norm_zero_to_four[1:4]
mean_previous_years_0_4  <- mean(previous_years_0_4)
sd_previous_years_0_4 <- sd(previous_years_0_4)
z_score_0_4 <- (normalised_deaths_for_calculations$norm_zero_to_four[5]-mean_previous_years_0_4)/sd_previous_years_0_4
p_value_0_4 <- pnorm(-abs(z_score_0_4))*2

#5-14
previous_years_5_14 <- normalised_deaths_for_calculations$norm_five_to_fourteen[1:4]
mean_previous_years_5_14  <- mean(previous_years_5_14)
sd_previous_years_5_14 <- sd(previous_years_5_14)
z_score_5_14 <- (normalised_deaths_for_calculations$norm_five_to_fourteen[5]-mean_previous_years_5_14)/sd_previous_years_5_14
p_value_5_14 <- pnorm(-abs(z_score_5_14))*2

