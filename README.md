Analysis of the indirect effects of the COVID-19 pandemic on severe paediatric morbidity and mortality in Scotland. 

medRxiv doi: https://www.medrxiv.org/content/10.1101/2020.10.15.20212308v1

Please contact me (thomas.christie.williams@ed.ac.uk) if you have any questions relating to the analyses or the datasets. 

<b>Notes on emergency healthcare utilisation data</b>
Normalised national data on emergency consultations, attendances and admissions in Scotland is available on this GitLab page. For raw counts, the data from 2018-20 is available from https://www.opendata.nhs.scot/group/covid-19, and notes on preparation and extraction are available from https://github.com/Public-Health-Scotland/covid-wider-impact.
The raw count data from 2016-2018 is available from Public Health Scotland on request. 

<b>Notes on mortality data</b>
This data is made freely available on this GitLab page, with the proviso from National Records of Scotland that the data from 2019 and 2020 remains provisional. 
